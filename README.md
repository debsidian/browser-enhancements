# browser-enhancements

The items in this repo have the following three and a half goals: 

- to make your Firefox experience more enjoyable 
- to make your Firefox experience more private 
- to make your Firefox experience more secure
- And the ½ part, attempting to make Brave Browser a *little* less shitty. No guarantees on this one.

That is all.
