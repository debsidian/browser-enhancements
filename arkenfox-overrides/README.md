These are the user-overrides which I use daily. There are two files, for two Firefox Profiles. A regular daily-driver profile and a profile for streaming video from subscription providers.

## user-overrides.js

This is my the overrides which is what I use on my daily driver profile. The overrides in this file do not weaken any of the hardening provided by stock Arkenfox. Instead, it makes the browser more user-friendly. I enable search in the URL bar (Although you have to manually set DuckDuckGo or other privacy-respecting search engine as your default search provider. AFAIK, there is no way to do that via `about:config` preferences), and other things which aim to make FF a better experience.

## streaming-overrides.js

This is more or less the same as the user-overrides but with 3 changes that are required for the streaming media sites to work properly:

1. Enabling EME (which enables DRM)
2. Disables RFP, while maintaining ETP at `Strict`
3. Changes X-Origin policy. In the stock Arkenfox, there is no referrer sent, ever. HBO Max, Discovery+ & Peacock require a referrer, so this override will simply make whatever the target URL is, as the referring URL. If you don't use one of these streaming services, feel free to disable this section of the overrides.

Supported video streaming platforms: (in alphabetical order)
- AETV
- AMC+
- Amazon Prime Video
- Discovery+
- Disney+
- HBO Max
- History
- Hoopla
- Hulu
- Kanopy
- Netflix
- PBS Passport
- Paramount+
- Peacock
- PlutoTV
- Sundance Now
- Tubi.tv
- YouTubeTV

If you have any questions, just ask.
