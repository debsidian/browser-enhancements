/*
* Name: Debsidian's subscription media streaming `user-overrides.js` for Arkenfox `user.js`
* Last updated: 10 November 2023
* Version 115
* Project goal: The goal of this `user-overrides.js` (to be used with Arkenfox's user.js https://github.com/arkenfox/user.js) is to create a hardened (as much as possible) Firefox capable of streaming DRM video from the major American streaming-video content providers. This should be a distinct profile, which you can create in `about:profiles`. The aim is not to make you anonymous whilst streaming. Presumably, you are logged into Netflix or another streaming account; anonymity is a moot issue. 
Ideally, this is used in combination with uBlock Origin on Medium mode. https://github.com/gorhill/uBlock/wiki/Blocking-mode:-medium-mode

NB: Pihole is often a problem when streaming video. uBlock Origin is properly set up (by default) to send a lot of `noop`s to the ad, analytics & tracking companies. If you use a VPN, it needs to be set to make sure it allows all content. uBO in medium mode will act as the https switchboard, allowing and disallowing network connections.

NB 2: You will need to rename this file to `user-overrides.js` for it to work

Thank you for coming to my Ted talk.

*******/

/* Re-enable sending `X-Origin Policy`. This is necessary for HBO Max, Discovery+ & Peacock (NBC) streaming to work. This will spoof the referrer header instead of disallowing the referrer header (which is the Arkenfox default). 
In this implementation, you won't be sending your actual (real) referrer, just the spoofed one.
*******/
user_pref("network.http.referer.XOriginPolicy", 0);
user_pref("network.http.referer.spoofSource", true); // Send the target URL as the referrer.

/* 
Revert RFP settings to `Strict` mode. This is necessary for Netflix, Paramount+, HBO Max & Discovery+ to work. But this is acceptable for the video streaming use-case because:
1. You are already identifiable because you are logged into a subscription streaming account. 
2. `Total cookie protection`, `dFPI`, `Multi-account Containers`, `Sanitize on close` and literally all other Arkenfox privacy enhancements are still enabled, so you're still incredibly private. 
*******/
user_pref("privacy.resistFingerprinting", false); //turn off the "resist fingerprinting". This shouldn't matter too much because you are streaming whilst logged into an account.
user_pref("privacy.window.maxInnerWidth", 1000); // Revert to default `Strict` setting
user_pref("privacy.window.maxInnerHeight", 1000); // Revert to default `Strict` setting
user_pref("privacy.resistFingerprinting.block_mozAddonManager", false); // Revert to default `Strict` setting
user_pref("privacy.resistFingerprinting.letterboxing", false); // Revert to default `Strict` setting
// user_pref("webgl.disabled", false); // Revert to default `Strict` setting

// Enable DRM in order to use music and video streaming services. This is a necessary setting for all streaming platforms.
user_pref("media.eme.enabled", true); // EME = Encryption Media Extension
user_pref("media.mediasource.webm.enabled", true); // Enables VP9 codec and 4K. This will reduce battery life and requires a newer, more powerful machine
// Everything below this line is optional. They are just my personal preferences to make FF a more enjoyable experience. Feel free to disable any or all of these preferences.

//Enable Global Privacy Control settings
user_pref("privacy.globalprivacycontrol.functionality.enabled", true);
user_pref("privacy.globalprivacycontrol.enabled", true);

//OpSec Settings
user_pref("signon.rememberSignons", false); // Disable Firefox from automatically saving passwords
user_pref("security.webauthn.ctap2", true); // Enables CTAP2 (FIDO2 support in the browser. Enables native Yubikey security
user_pref("dom.event.clipboardevents.enabled", false); // Force the use of “paste” functionality even if a site disables it. Primarily used to enable password managers.

// URLbar settings
user_pref("keyword.enabled", true); // Enable search in URLbar
user_pref("browser.urlbar.suggest.engines", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.openpage", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.topsites", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.bookmark", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.shortcuts.history", false); // Don't create extra shortcuts
user_pref("browser.urlbar.shortcuts.tabs", false); // Don't create extra shortcuts
user_pref("browser.urlbar.update2.engineAliasRefresh", true); // Creates ability to add custom search engines in Firefox.
user_pref("browser.urlbar.suggest.engines", false); // Don't suggest search engines

// Quiet startup without pinging a million places. Recommend setting search to Startpage or DDG.
user_pref("browser.startup.page", 1);
user_pref("browser.startup.homepage", "about:home");
user_pref("browser.newtabpage.enabled", true);

// Miscellaneous
user_pref("ui.systemUsesDarkTheme", 1); // Dark mode. Always.
user_pref("general.autoScroll", true); // Enable autoscroll in General Preferences
user_pref("browser.toolbars.bookmarks.visibility", "always"); // Keep the bookmark toolbars in view all the time
user_pref("security.enterprise_roots.enabled", true); // Trust NextDNS certificates

// Firefox annoyances 
user_pref("browser.tabs.tabmanager.enabled", false); // Remove tab-manager arrow in top right of window border bar
user_pref("browser.privatebrowsing.enable-new-indicator", false); // Remove the giant "Private Browsing" banner text in PB windows.
user_pref("full-screen-api.warning.timeout", 0); // Remove fullscreen message annoyance
user_pref("browser.warnOnQuitShortcut", false); // Don't warn me about open tabs when quitting Firefox

// I hate Pocket
user_pref("extensions.pocket.enabled", false); // Disable Pocket [Formerly Section 9000]
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false); // Recommended by Pocket Part 1
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false); // Recommended by Pocket Part 2
user_pref("extensions.pocket.showHome", false); // Don't show Pocket stuff on the about:home page

// Overrides Parrot
user_pref("_overrides.parrot", "overrides: success"); // Confirmation that the user-overrides.js was successfully appended to your user.js and written to prefs.js
