// My Thunderbird user.js

/* TO RESET/REMOVE/DELETE A PREFERENCE:
 * ------------------------------------
 * 1. exit Thunderbird
 * 2. comment out the preference(s) by prefixing it with 2 forward slashes (//)
 *    and save your changes (do not move it to the DEPRECIATED/REMOVED PREFS
 *    section below)
 * 3. copy this entire file to the very end of the 'HorlogeSkynet' user.js
 * 4. run the 'ghacks' prefsCleaner script
 *
 * TO CHANGE THE VALUE OF A PREFERENCE:
 * ------------------------------------
 * 1. exit Thunderbird
 * 2. copy the entire preference line to the CUSTOM CODE section below
 * 3. change the preference value and save your changes
 * 4. copy this entire file to the very end of the 'HorlogeSkynet' user.js
 *
 * TO FIND THE DEFAULT VALUE OF A PREFERENCE:
 * ------------------------------------------
 * 1. find the preference in Thunderbird > Preferences > Advanced > Config
 * Editor
 * 2. right click the preference and select 'Reset'
 * note that not all preferences are listed in the Config Editor
 */


user_pref("network.cookie.cookieBehavior", 2);
user_pref("network.http.sendRefererHeader", 0);
user_pref("privacy.firstparty.isolate", true);
user_pref("calendar.timezone.local", "America/Los_Angeles");
user_pref("mailnews.start_page.url", "");


// Disable Google Safe Browsing
user_pref("browser.safebrowsing.blockedURIs.enabled", false);
user_pref("browser.safebrowsing.downloads.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous", false);
user_pref("browser.safebrowsing.downloads.remote.block_dangerous_host", false);
user_pref("browser.safebrowsing.downloads.remote.block_potentially_unwanted", false);
user_pref("browser.safebrowsing.downloads.remote.block_uncommon", false);
user_pref("browser.safebrowsing.downloads.remote.enabled", false);
user_pref("browser.safebrowsing.downloads.remote.url", "");
user_pref("browser.safebrowsing.malware.enabled", false);
user_pref("browser.safebrowsing.phishing.enabled", false);

// Telemetry
user_pref("toolkit.telemetry.unified", false);
user_pref("toolkit.telemetry.enabled", false); // see [NOTE]
user_pref("toolkit.telemetry.server", "data:,");
user_pref("toolkit.telemetry.archive.enabled", false);
user_pref("toolkit.telemetry.newProfilePing.enabled", false); // [FF55+]
user_pref("toolkit.telemetry.shutdownPingSender.enabled", false); // [FF55+]
user_pref("toolkit.telemetry.updatePing.enabled", false); // [FF56+]
user_pref("toolkit.telemetry.bhrPing.enabled", false); // [FF57+] Background Hang Reporter
user_pref("toolkit.telemetry.firstShutdownPing.enabled", false); // [FF57+]
/* 0333: disable Telemetry Coverage
 * [1] https://blog.mozilla.org/data/2018/08/20/effectively-measuring-search-in-firefox/ ***/
user_pref("toolkit.telemetry.coverage.opt-out", true); // [HIDDEN PREF]
user_pref("toolkit.coverage.opt-out", true); // [FF64+] [HIDDEN PREF]
user_pref("toolkit.coverage.endpoint.base", "");
/* 0334: disable PingCentre telemetry (used in several System Add-ons) [FF57+]
 * Defense-in-depth: currently covered by 0331 ***/
user_pref("browser.ping-centre.telemetry", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);
user_pref("browser.crashReports.unsubmittedCheck.autoSubmit2", true);
user_pref("breakpad.reportURL", "");

//Misc
user_pref("captivedetect.canonicalURL", "");
user_pref("network.captive-portal-service.enabled", false); // [FF52+]
/* 0361: disable Network Connectivity checks [FF65+]
 * [1] https://bugzilla.mozilla.org/1460537 ***/
user_pref("network.connectivity-service.enabled", false);
/* 0370: disable UI instrumentation ***/
user_pref("mail.instrumentation.postUrl", "");
user_pref("mail.instrumentation.askUser", false);
user_pref("mail.instrumentation.userOptedIn", false);
user_pref("general.useragent.override", ""); // Don't send a user-agent like Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Thunderbird/102.6.0
user_pref("extensions.getAddons.cache.enabled", false); // Don't send daily metadata to Mozilla RE add-ons
user_pref("mailnews.start_page.enabled", false); // Disable Mozilla Startup page

//Geolocation
user_pref("geo.provider.use_corelocation", false); // [MAC]
user_pref("geo.provider.use_gpsd", false); // [LINUX]
user_pref("network.prefetch-next", false);
user_pref("network.dns.disablePrefetch", true);
user_pref("network.predictor.enabled", false);
user_pref("network.predictor.enable-prefetch", false); // [FF48+] [DEFAULT: false]
user_pref("network.http.speculative-parallel-limit", 0);
user_pref("browser.meta_refresh_when_inactive.disabled", true);
user_pref("security.ssl.require_safe_negotiation", true);
user_pref("_user.js.parrot", "2600 syntax error: the parrot's run down the curtain!");
/* 2601: prevent accessibility services from accessing your browser [RESTART]
 * [1] https://support.mozilla.org/kb/accessibility-services ***/
user_pref("accessibility.force_disabled", 1);
/* 2602: disable sending additional analytics to web servers
 * [1] https://developer.mozilla.org/docs/Web/API/Navigator/sendBeacon ***/
user_pref("beacon.enabled", false);
/* 2603: remove temp files opened with an external application
 * [1] https://bugzilla.mozilla.org/302433 ***/
user_pref("browser.helperApps.deleteTempFileOnExit", true);
/* 2604: disable page thumbnail collection ***/
user_pref("browser.pagethumbnails.capturing_disabled", true); // [HIDDEN PREF]
/* 2606: disable UITour backend so there is no chance that a remote page can use it ***/
user_pref("browser.uitour.enabled", false);
user_pref("browser.uitour.url", "");
/* 2607: disable various developer tools in browser context
 * [1] https://github.com/pyllyukko/user.js/issues/179#issuecomment-246468676 ***/
user_pref("devtools.chrome.enabled", false);
/* 2608: reset remote debugging to disabled
 * [1] https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/16222 ***/
user_pref("devtools.debugger.remote-enabled", false); // [DEFAULT: false]
/* 2611: disable middle mouse click opening links from clipboard
 * [1] https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/10089 ***/
user_pref("middlemouse.contentLoadURL", false);
/* 2616: remove special permissions for certain mozilla domains [FF35+]
 * [1] resource://app/defaults/permissions ***/
user_pref("permissions.manager.defaultsUrl", "");
/* 2617: remove webchannel whitelist ***/
user_pref("webchannel.allowObject.urlWhitelist", "");
/* 2619: use Punycode in Internationalized Domain Names to eliminate possible spoofing
 * [SETUP-WEB] Might be undesirable for non-latin alphabet users since legitimate IDN's are also punycoded
 * [TEST] https://www.xn--80ak6aa92e.com/ (www.apple.com)
 * [1] https://wiki.mozilla.org/IDN_Display_Algorithm
 * [2] https://en.wikipedia.org/wiki/IDN_homograph_attack
 * [3] https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=punycode+firefox
 * [4] https://www.xudongz.com/blog/2017/idn-phishing/ ***/
user_pref("network.IDN_show_punycode", true);
/* 2620: enforce PDFJS, disable PDFJS scripting [SETUP-CHROME]
 * This setting controls if the option "Display in Thunderbird" is available in the setting below
 *   and by effect controls whether PDFs are handled in-browser or externally ("Ask" or "Open With")
 * PROS: pdfjs is lightweight, open source, and more secure/vetted than most
 *   Exploits are rare (one serious case in seven years), treated seriously and patched quickly.
 *   It doesn't break "state separation" of browser content (by not sharing with OS, independent apps).
 *   It maintains disk avoidance and application data isolation. It's convenient. You can still save to disk.
 * CONS: You may prefer a different pdf reader for security reasons
 * CAVEAT: JS can still force a pdf to open in-browser by bundling its own code ***/
user_pref("pdfjs.disabled", false); // [DEFAULT: false]
user_pref("pdfjs.enableScripting", false); // [FF86+]
/* 2621: disable links launching Windows Store on Windows 8/8.1/10 [WINDOWS] ***/
user_pref("network.protocol-handler.external.ms-windows-store", false);
/* 2623: disable permissions delegation [FF73+]
 * Currently applies to cross-origin geolocation, camera, mic and screen-sharing
 * permissions, and fullscreen requests. Disabling delegation means any prompts
 * for these will show/use their correct 3rd party origin
 * [1] https://groups.google.com/forum/#!topic/mozilla.dev.platform/BdFOMAuCGW8/discussion ***/
user_pref("permissions.delegation.enabled", false);

/** DOWNLOADS ***/
/* 2651: enable user interaction for security by always asking where to download
 * [SETUP-CHROME] On Android this blocks longtapping and saving images ***/
user_pref("browser.download.useDownloadDir", false);
/* 2653: disable adding downloads to the system's "recent documents" list ***/
user_pref("browser.download.manager.addToRecentDocs", false);
/* 2654: enable user interaction for security by always asking how to handle new mimetypes [FF101+]
 * [SETTING] General>Files and Applications>What should Thunderbird do with other files ***/
user_pref("browser.download.always_ask_before_handling_new_types", true);
user_pref("privacy.partition.serviceWorkers", true);
user_pref("network.cookie.lifetimePolicy", 2);
/* 2802: delete cache on exit [FF96+]
 * [NOTE] We already disable disk cache (1001) and clear on exit (2811) which is more robust
 * [1] https://bugzilla.mozilla.org/1671182 ***/
   // user_pref("privacy.clearsitedata.cache.enabled", true);

/** SANITIZE ON SHUTDOWN : ALL OR NOTHING ***/
/* 2810: enable Thunderbird to clear items on shutdown (2811)
 * [SETTING] Privacy & Security>History>Custom Settings>Clear history when Thunderbird closes ***/
user_pref("privacy.sanitize.sanitizeOnShutdown", true);
/* 2811: set/enforce what items to clear on shutdown (if 2810 is true) [SETUP-CHROME]
 * These items do not use exceptions, it is all or nothing (1681701)
 * [NOTE] If "history" is true, downloads will also be cleared
 * [NOTE] "sessions": Active Logins: refers to HTTP Basic Authentication [1], not logins via cookies
 * [NOTE] "offlineApps": Offline Website Data: localStorage, service worker cache, QuotaManager (IndexedDB, asm-cache)
 * [1] https://en.wikipedia.org/wiki/Basic_access_authentication ***/
user_pref("privacy.clearOnShutdown.cache", true);     // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.downloads", true); // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.formdata", true);  // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.history", true);   // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.sessions", true);  // [DEFAULT: true]
user_pref("privacy.clearOnShutdown.offlineApps", true); // [DEFAULT: false]
user_pref("privacy.clearOnShutdown.cookies", true);
   // user_pref("privacy.clearOnShutdown.siteSettings", false);

//Headers
/** HEADERS ***/
/* 9120:
 * true=Show Sender header in message pane.
 * false=Does nothing. (default) ***/
user_pref("mailnews.headers.showSender", true);
/* 9121:
 * true=Show User Agent header in message pane
 * false=Does nothing. (default) ***/
user_pref("mailnews.headers.showUserAgent", true);
/* 9122: Hello argument
 * Lets you replace your IP address with the specified string in Received: headers when your
 * IP address is not a "fully qualified domain name" (FQDN). Typically you only need to do this
 * when you have a NAT box to prevent it from using the NAT boxes IP address.
 * If you don't set it to something in your SMTP server's domain it may increase your spam
 * score. ***/
user_pref("mail.smtpserver.default.hello_argument", "[127.0.0.1]");

//Address Book
user_pref("mail.collect_email_address_outgoing", false);

/*** [SECTION 9200]: EMAIL COMPOSITION (ENCODING / FORMAT / VIEW)
   Options that relate to composition, formatting and viewing email
***/
user_pref("_user.js.parrot", "9200 syntax error: this parrot has got no mail!");

/** ENCODING ***/
/* 9201: Prevent fallback encoding to windows-1252, prefer 7bit or 8bit UTF-8
 * [1] http://forums.mozillazine.org/viewtopic.php?f=28&t=267341
 * [2] https://bugzilla.mozilla.org/show_bug.cgi?id=214729
 * [3] https://stackoverflow.com/a/28531705 ***/
user_pref("intl.fallbackCharsetList.ISO-8859-1", "UTF-8");
/* 9202: Set encoding of incoming mail ***/
user_pref("mailnews.view_default_charset", "UTF-8");
/* 9203: Set encoding of outgoing mail ***/
user_pref("mailnews.send_default_charset", "UTF-8");
/* 9204: Forces encoding in reply to be the default charset
 * [1] https://bugzilla.mozilla.org/show_bug.cgi?id=234958#c2 ***/
user_pref("mailnews.reply_in_default_charset", true);
/* 9205: Avoid information leakage in reply header
 * Reply header may contain sensitive information about system locale (date and/or language)
 * 0=no header
 * 1="<author> wrote:" (see `reply_header_authorwrotesingle` below)
 * 2="On <date> <author> wrote:" (see `reply_header_ondateauthorwrote` below [DEFAULT])
 * 3="<author> wrote On <date>:" (see `reply_header_authorwroteondate` below`)
 * 4=user specified (you may use below tokens to forge your own format [DISCOURAGED]) ***/
// user_pref("mailnews.reply_header_type", 1);
// user_pref("mailnews.reply_header_authorwrotesingle", "#1 wrote:");
   // user_pref("mailnews.reply_header_ondateauthorwrote", "On #2 #3, #1 wrote:");
   // user_pref("mailnews.reply_header_authorwroteondate", "#1 wrote on #2 #3:");
/* 9206: Prevent spellchecking dictionary leakage through Content-Language header
 * [1] https://bugzilla.mozilla.org/show_bug.cgi?id=1370217 ***/
user_pref("mail.suppress_content_language", true);
/* 9207: Sanitize Date header to convert date to UTC and round to closest minute
 * [1] https://bugzilla.mozilla.org/show_bug.cgi?id=1603359 ***/

/** COMPOSITION ***/
/* 9214: What classes can process incoming data.
 * (0=All classes (default), 1=Don't display HTML, 2=Don't display HTML and inline images,
 * 3=Don't display HTML, inline images and some other uncommon types, 100=Use a hard coded list)
 * In the past this has mitigated a vulnerability CVE-2008-0304 (rare)
 * [1] https://www.mozilla.org/en-US/security/advisories/mfsa2008-12/
 * [2] https://bugzilla.mozilla.org/show_bug.cgi?id=677905 ***/
// user_pref("mailnews.display.disallow_mime_handlers", 3);
/* 9215: How to display HTML parts of a message body
 * (0=Display the HTML normally (default), 1=Convert it to text and then back again
 * 2=Display the HTML source, 3=Sanitize the HTML, 4=Display all body parts)
 * (in trunk builds later than 2011-07-23)
 * [1] https://bugzilla.mozilla.org/show_bug.cgi?id=602718
 * [2] https://hg.mozilla.org/comm-central/rev/c1ef44a22eb2
 * [3] https://www.bucksch.org/1/projects/mozilla/108153/ ***/
// user_pref("mailnews.display.html_as", 3);
/* 9216: Prefer to view as plaintext or HTML [SETUP-FEATURE]
 * true=Display a message as plain text when there is both a HTML and a plain
 * text version of a message body
 * false=Display a message as HTML when there is both a HTML and a plain text
 * version of a message body. (default) ***/
// user_pref("mailnews.display.prefer_plaintext", false);
/* 9217: Inline attachments [SETUP-FEATURE]
 // * true=Show inlinable attachments (text, images, messages) after the message.
 * false=Do not display any attachments with the message ***/
// user_pref("mail.inline_attachments", false);
/* 9218: Big attachment warning
 * [1] https://support.mozilla.org/en-US/questions/1081046
 * [2] http://forums.mozillazine.org/viewtopic.php?f=39&t=2949521 */
user_pref("mail.compose.big_attachments.notify", true); // [DEFAULT: true]
/* 9219: Set big attachment size to warn at */
user_pref("mail.compose.big_attachments.threshold_kb", 9220); // [DEFAULT: 5120]
   // user_pref("mailnews.message_warning_size", 20971520); // [DEFAULT: 20971520]
/* 9220: Set public recipients number from which BCC is advised ***/
   // user_pref("mail.compose.warn_public_recipients.threshold", 15); // [DEFAULT: 15]
/* 9221: Show an alert if the warning above has not been addressed ***/
user_pref("mail.compose.warn_public_recipients.aggressive", true);
/* 9222: Disable link previews
 * [SETTING] Composition > Composition > Add link previews when pasting URLs ***/
user_pref("mail.compose.add_link_preview", false); // [DEFAULT: false]

/** VIEW ***/
/* 9230: Disable JavaScript
 * [NOTE] JavaScript is already disabled in message content.
 * [1] https://developer.mozilla.org/en-US/docs/Mozilla/Thunderbird/Releases/3
 * [2] https://stackoverflow.com/questions/3054315/is-javascript-supported-in-an-email-message
 * ***/
user_pref("javascript.enabled", true);
/* 9231: Disable media source extensions
 * [1] https://www.ghacks.net/2014/05/10/enable-media-source-extensions-firefox ***/
user_pref("media.mediasource.enabled", false);
/* 9232: Disable hardware decoding support ***/
user_pref("media.hardware-video-decoding.enabled", false);
/* 9233: Default image permissions
 * 1=Allow all images to load, regardless of origin. (Default),
 * 2=Block all images from loading.
 * 3=Prevent third-party images from loading
 * [1] http://kb.mozillazine.org/Permissions.default.image ***/
user_pref("permissions.default.image", 2);
/* 9240: Builtin phishing/scamming detection
 * [NOTE] These preferences are enabled by default and should not usually be touched.
 * [1] https://searchfox.org/comm-central/source/mail/modules/PhishingDetector.jsm ***/
user_pref("mail.phishing.detection.enabled", true);
user_pref("mail.phishing.detection.disallow_form_actions", true);
user_pref("mail.phishing.detection.ipaddresses", true);
user_pref("mail.phishing.detection.mismatched_hosts", true);
/* 9250: Disable remote content loading
 * [SETTING] Privacy & Security > Privacy > Mail content > Allow remote content in messages ***/
user_pref("mailnews.message_display.disable_remote_image", true); // [DEFAULT: true]

/*** [SECTION 9300]: OTHER THUNDERBIRD COMPONENTS (CHAT / CALENDAR / RSS)
   Options that relate to other Thunderbird components such as the chat client, calendar and RSS)
***/
user_pref("_user.js.parrot", "9300 syntax error: this parrot is not tweeting!");

/** CHAT ***/
/* 9301: Disable chat functionality [SETUP-FEATURE] ***/
user_pref("mail.chat.enabled", false);
/* 9302: Disable logging of group chats ***/
user_pref("purple.logging.log_chats", false);
/* 9303: Disable logging of 1 to 1 conversations ***/
user_pref("purple.logging.log_ims", false);
/* 9304: Disable logging of system messages ***/
user_pref("purple.logging.log_system", false);
/* 9305: Disable typing notifications ***/
user_pref("purple.conversations.im.send_typing", false);
/* 9306: When chat is enabled, do not connect to accounts automatically
 * 0=Do not connect / show the account manager,
 * 1=Connect automatically. (Default) ***/
   // user_pref("messenger.startup.action", 0);
/* 9307: When chat is enabled, do not report idle status ***/
   // user_pref("messenger.status.reportIdle", false);
/* 9308: Disable chat desktop notifications ***/
   // user_pref("mail.chat.show_desktop_notifications", false);
/* 9309: Decide how much information will be shown in chat notifications
 * 0=Show all info (sender, chat message message preview),
 * 1=Show sender's info only (not message preview),
 * 2=No info (fill dummy values). ***/
user_pref("mail.chat.notification_info", 2);

/** CALENDAR ***/
/* 9312: Set calendar timezone to avoid system detection [SETUP-INSTALL]
 * By default, extensive system detection would be performed to find user's current timezone.
 * Setting this preference to "UTC" should disable it.
 * You may also directly set it to your timezone, i.e. "Pacific/Fakaofo"
 * [SETTING] Calendar > Calendar > Timezone ***/
/* 9313: Disable calendar service performing event "extraction" from email content ***/
user_pref("calendar.extract.service.enabled", true);  // [DEFAULT: false]

/** RSS ***/
/** These features don't actually do anything as they aren't implemented
 * [1] https://searchfox.org/comm-esr102/source/mail/base/content/mailWindowOverlay.js#1082
 * [2] https://bugzilla.mozilla.org/show_bug.cgi?id=458606#c9
/* 9320: What classes can process incoming data.
 * (0=All classes (default), 1=Don't display HTML, 2=Don't display HTML and inline images,
 * 3=Don't display HTML, inline images and some other uncommon types, 100=Use a hard coded list)
 * [1] https://www.privacy-handbuch.de/handbuch_31j.htm
user_pref("rss.display.disallow_mime_handlers", 3);
/* 9321: How to display HTML parts of a message body
 * (0=Display the HTML normally (default), 1=Convert it to text and then back again
 * 2=Display the HTML source, 3=Sanitize the HTML, 4=Display all body parts)
 * (in trunk builds later than 2011-07-23)
 * [1] https://bugzilla.mozilla.org/show_bug.cgi?id=602718
 * [2] https://hg.mozilla.org/comm-central/rev/c1ef44a22eb2
 * [3] https://www.bucksch.org/1/projects/mozilla/108153/
user_pref("rss.display.html_as", 1);
/* 9322: Prefer to view as plaintext or HTML
 * true=Display a message as plain text when there is both a HTML and a plain
 * text version of a message body
 * false=Display a message as HTML when there is both a HTML and a plain text
 * version of a message body. (default)
user_pref("rss.display.prefer_plaintext", true);
**/
/* 9323: Feed message display (summary or web page), on open.
 * Action on double click or enter in threadpane for a feed message.
 * 0=open content-base url in new window, 1=open summary in new window,
 * 2=toggle load summary and content-base url in message pane,
 * 3=load content-base url in browser
 * [1] http://forums.mozillazine.org/viewtopic.php?f=39&t=2502335 ***/
user_pref("rss.show.content-base", 3);
/* 9324: Feed message display (summary or web page), on select.
 * 0=global override, load web page, 1=global override, load summary,
 * 2=use default feed folder setting from Subscribe dialog; if no setting default to 1 ***/
user_pref("rss.show.summary", 1);
/* 9325: Feed message additional web page display.
 * 0=no action, 1=load web page in default browser, on select ***/
user_pref("rss.message.loadWebPageOnSelect", 0);

/*** [SECTION 9400]: THUNDERBIRD ENCRYPTION (OPENPGP / GNUPG)
   Options that relate to e-mail encryption in Thunderbird.
   [1] https://wiki.mozilla.org/Thunderbird:OpenPGP
   [2] https://support.mozilla.org/kb/openpgp-thunderbird-howto-and-faq
***/
user_pref("_user.js.parrot", "9400 syntax error: this parrot is talking in codes!");

/** OPENPGP ***/
/* 9400: disable OpenPGP "encryption is possible" reminder ***/
   // user_pref("mail.openpgp.remind_encryption_possible", false); // [TB102+]

/** GNUPG ***/
/* 9409: Allow the use of external GnuPG
 * Whenever RNP fails to decrypt a message, Thunderbird will tray against system GnuPG
 * [1] https://wiki.mozilla.org/Thunderbird:OpenPGP:Smartcards#Allow_the_use_of_external_GnuPG ***/
user_pref("mail.openpgp.allow_external_gnupg", true);  // [HIDDEN PREF]

/*** [SECTION 9999]: DEPRECATED / REMOVED / LEGACY / RENAMED
   Documentation denoted as [-]. Items deprecated prior to FF91 have been archived at [1]
   [1] https://github.com/arkenfox/user.js/issues/123
***/
user_pref("_user.js.parrot", "9999 syntax error: the parrot's shuffled off 'is mortal coil!");

user_pref("_user.js.parrot", "SUCCESS: No no he's not dead, he's, he's restin'!");



user_pref("mail.chat.play_sound", false);
user_pref("mail.chat.show_desktop_notifications", false);
user_pref("mail.collect_email_address_outgoing", false);
user_pref("mail.compose.autosave", false);
