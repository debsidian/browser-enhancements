/*
* Name: Debsidian's daily driver `user-overrides.js` for Arkenfox `user.js`
* Last updated: 24 November 2023
* Version 120
* Note: Disables RFP. Intended to be used with uBlock Origin on hard mode and also with CanvasBlock

*******/


// Revert RFP settings to `Strict` mode
user_pref("privacy.resistFingerprinting", false); //turn off the "resist fingerprinting".
user_pref("privacy.window.maxInnerWidth", 1000); // Revert to default `Strict` setting
user_pref("privacy.window.maxInnerHeight", 1000); // Revert to default `Strict` setting
user_pref("privacy.resistFingerprinting.letterboxing", false); // Revert to default `Strict` setting
// user_pref("webgl.disabled", false); // Revert to default `Strict` setting

// Enable DRM in order to use music and video streaming services.
user_pref("media.eme.enabled", true); // EME = Encryption Media Extension
user_pref("image.mem.decode_bytes_at_a_time", 32768); // Increase size of image cache

//Network
user_pref("network.buffer.cache.size", 262144);
user_pref("network.buffer.cache.count", 128);
user_pref("network.http.max-connections", 1800);
user_pref("network.http.max-persistent-connections-per-server", 10);
user_pref("network.http.max-urgent-start-excessive-connections-per-host", 5);
user_pref("network.http.pacing.requests.enabled", false);
user_pref("network.dnsCacheExpiration", 3600);
user_pref("network.dns.max_high_priority_threads", 8);
user_pref("network.ssl_tokens_cache_capacity", 10240);

// GFX
user_pref("gfx.canvas.accelerated.cache-items", 4096);
user_pref("gfx.canvas.accelerated.cache-size", 512);
user_pref("gfx.content.skia-font-cache-size", 20);

// Enable Global Privacy Control settings
user_pref("privacy.globalprivacycontrol.functionality.enabled", true);
user_pref("privacy.globalprivacycontrol.enabled", true);

//OpSec Settings
user_pref("signon.rememberSignons", false); // Disable Firefox from automatically saving passwords
user_pref("security.webauthn.ctap2", true); // Enables CTAP2 (FIDO2 support in the browser. Enables native browser Yubikey security
user_pref("extensions.formautofill.addresses.enabled", false); // Disable addresses autofill
user_pref("extensions.formautofill.creditCards.enabled", false); //Disable credit card autofill

// URLbar settings
user_pref("keyword.enabled", true); // Enable search in URLbar
user_pref("browser.urlbar.suggest.engines", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.openpage", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.topsites", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.suggest.bookmark", false); // Don't auto-suggest stuff
user_pref("browser.urlbar.update2.engineAliasRefresh", true); // Creates ability to add custom search engines in Firefox.

// Quiet startup without pinging a million places.
user_pref("browser.startup.page", 1);
user_pref("browser.startup.homepage", "about:home");
user_pref("browser.newtabpage.enabled", true);

// Miscellaneous
user_pref("content.notify.interval", 100000); // Begin to render the page even if the whole page hasn't been downloaded yet
user_pref("ui.systemUsesDarkTheme", 1); // Dark mode. Always.
user_pref("general.autoScroll", true); // Enable autoscroll in General Preferences
user_pref("browser.toolbars.bookmarks.visibility", "always"); // Keep the bookmark toolbars in view all the time
user_pref("security.enterprise_roots.enabled", true); // Trust NextDNS certificates

// Firefox annoyances 
user_pref("browser.tabs.tabmanager.enabled", false); // Remove tab-manager arrow in top right of window border bar
user_pref("browser.privatebrowsing.enable-new-indicator", false); // Remove the giant "Private Browsing" banner text in PB windows.
user_pref("browser.warnOnQuitShortcut", false); // Don't warn me about open tabs when quitting Firefox
/** MOZILLA UI ***/
user_pref("browser.privatebrowsing.vpnpromourl", "");
user_pref("extensions.getAddons.showPane", false);
user_pref("extensions.htmlaboutaddons.recommendations.enabled", false);
user_pref("browser.discovery.enabled", false);
user_pref("browser.shell.checkDefaultBrowser", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.addons", false);
user_pref("browser.newtabpage.activity-stream.asrouter.userprefs.cfr.features", false);
user_pref("browser.preferences.moreFromMozilla", false);
user_pref("browser.tabs.tabmanager.enabled", false);
user_pref("browser.aboutConfig.showWarning", false);
user_pref("browser.aboutwelcome.enabled", false);

/** THEME ADJUSTMENTS ***/
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);
user_pref("browser.compactmode.show", true);
user_pref("browser.display.focus_ring_on_anything", true);
user_pref("browser.display.focus_ring_style", 0);
user_pref("browser.display.focus_ring_width", 0);
user_pref("layout.css.prefers-color-scheme.content-override", 2);
user_pref("browser.privateWindowSeparation.enabled", false); // WINDOWS

/** COOKIE BANNER HANDLING ***/
user_pref("cookiebanners.service.mode", 1);
user_pref("cookiebanners.service.mode.privateBrowsing", 1);
user_pref("cookiebanners.service.enableGlobalRules", true);

/** FULLSCREEN NOTICE ***/
user_pref("full-screen-api.transition-duration.enter", "0 0");
user_pref("full-screen-api.transition-duration.leave", "0 0");
user_pref("full-screen-api.warning.delay", -1);
user_pref("full-screen-api.warning.timeout", 0);

/** URL BAR ***/
user_pref("browser.urlbar.suggest.calculator", true);
user_pref("browser.urlbar.unitConversion.enabled", true);
user_pref("browser.urlbar.trending.featureGate", false);

/** DOWNLOADS ***/
user_pref("browser.download.useDownloadDir", false);
user_pref("browser.download.always_ask_before_handling_new_types", true);
user_pref("browser.download.manager.addToRecentDocs", false);

/** PDF ***/
user_pref("browser.download.open_pdf_attachments_inline", true);

/** TAB BEHAVIOR ***/
user_pref("browser.bookmarks.openInTabClosesMenu", false);
user_pref("browser.menu.showViewImageInfo", true);
user_pref("findbar.highlightAll", true);
user_pref("layout.word_select.eat_space_to_next_word", false);

// I hate Pocket
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false); // Disable "Recommended by Pocket" Part 1
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false); // Disable "Recommended by Pocket" Part 2
user_pref("extensions.pocket.showHome", false); // Don't show Pocket stuff on the about:home page

// Overrides Parrot
user_pref("_overrides.parrot", "overrides: success"); // Confirmation that the user-overrides.js was successfully appended to your user.js and written to prefs.js


