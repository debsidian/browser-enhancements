/*
* name: Debsidian's overrides to arkenfox user.js
* date: 10 November 2023
* Firefox version 119
* Note: RFP is enabled. Strict Arkenfox

    - 1st run `./updater.sh` to update all this stuff to the next version automatically. Append ` -e` after the .sh to make it ESR specific.
    - 2nd run `./prefsCleaner.sh` to reset prefs made inactive, including deprecated

[SECTION 0100]: STARTUP ***/
user_pref("browser.startup.page", 1); // Make browser startup screen Home screen. [0102]
user_pref("browser.startup.homepage", "about:home"); // Define what the homescreen url [103]
user_pref("browser.newtabpage.enabled", true); // Make new tabs == startup screen[104]
user_pref("browser.link.open_newwindow.restriction", 2); // I don't like that a new window is forcibly opened fullsize [4513]

// URLbar settings
user_pref("keyword.enabled", true); // Enable search in URLbar [801]
user_pref("browser.urlbar.suggest.engines", false); // Don't auto-suggest stuff [808]
user_pref("browser.urlbar.suggest.openpage", false); // Don't auto-suggest stuff [5010]
user_pref("browser.urlbar.suggest.topsites", false); // Don't auto-suggest stuff [5010]
user_pref("browser.urlbar.suggest.bookmark", false); // Don't auto-suggest stuff [5010]
user_pref("browser.urlbar.shortcuts.history", false); // Don't create extra shortcuts
user_pref("browser.urlbar.shortcuts.tabs", false); // Don't create extra shortcuts
user_pref("browser.urlbar.update2.engineAliasRefresh", true); // Creates ability to add custom search engines in Firefox. [Hidden preference. This is not a pref in Arkenfox]
user_pref("browser.urlbar.suggest.engines", false); // Don't suggest search engines

// MEDIA
user_pref("media.autoplay.default", 5); // Disable autoplay of HTML5 media [2030]

// [SECTION 5000] OPSEC
user_pref("signon.rememberSignons", false); // Disable Firefox from automatically saving passwords [5003]
user_pref("security.webauthn.ctap2", true); // Enables CTAP2 (FIDO2 support in the browser. Enables native Yubikey security

// Global Privacy Control settings
user_pref("privacy.globalprivacycontrol.functionality.enabled", true); // Enable GPC, part 1
user_pref("privacy.globalprivacycontrol.enabled", true); // Enable GPC, part 2

// Remove Firefox annoyances
user_pref("browser.tabs.tabmanager.enabled", false); // Remove tab-manager arrow in top right of window border bar
user_pref("browser.privatebrowsing.enable-new-indicator", false); // Remove the giant "Private Browsing" banner in PB windows
user_pref("browser.warnOnQuitShortcut", false); // Don't warn me about open tabs when quitting Firefox

// Work overrides. This should be a separate profile
// user_pref("network.automatic-ntlm-auth.allow-non-fqdn", true); // Enable SSO for hostnames

// Misc Preferences
user_pref("ui.systemUsesDarkTheme", 1); // Enable Dark mode only [Formerly Section 9000]
user_pref("identity.fxaccounts.enabled", false); // Disable Firefox Accounts & Sync [Formerly Section 9000]
user_pref("general.autoScroll", true); // Enable autoscroll in General Preferences
user_pref("browser.toolbars.bookmarks.visibility", "always"); // Always show the bookmark toolbar
user_pref("security.enterprise_roots.enabled", true); // Trust NextDNS certificates

// I hate Pocket
user_pref("extensions.pocket.enabled", false); // Disable Pocket [Formerly Section 9000]
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false); // Disable "Recommended by Pocket" Part 1
user_pref("browser.newtabpage.activity-stream.section.highlights.includePocket", false); // Disable "Recommended by Pocket" Part 2
user_pref("extensions.pocket.showHome", false); // Don't show Pocket stuff on about:home

// Confirmation
user_pref("_overrides.parrot", "overrides: success"); // Confirmation that the user-overrides.js was successfully appended to your user.js and written to prefs.js
